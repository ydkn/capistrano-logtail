# frozen_string_literal: true

require 'capistrano/logtail/utility'

load File.expand_path('logtail/tasks/logtail.cap', __dir__)
