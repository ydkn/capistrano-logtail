# frozen_string_literal: true

# Capistrano
module Capistrano
  # Logtail
  module Logtail
    # gem version
    VERSION = '0.1.1'
  end
end
