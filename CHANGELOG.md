# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.1] - 2015-05-20
### Changed
- using -F instead of -f for tail

## [0.1.0] - 2015-04-30
### Added
- tailing of all logs, rails log and custom log file
- allow to specify default log files and number of existing lines to show
